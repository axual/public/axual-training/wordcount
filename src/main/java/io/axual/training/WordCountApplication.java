package io.axual.training;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Properties;

public class WordCountApplication {
    private static final Logger LOG = LoggerFactory.getLogger(WordCountApplication.class);

    public static void main(String... args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "wordcount-application");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "broker:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        StreamsBuilder builder = new StreamsBuilder();

        KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore("wordcounts-store");
        Materialized<String, Long, KeyValueStore<Bytes, byte[]>> materializedWordCounts = Materialized.as(storeSupplier);

        KStream<String, String> textLines = builder.stream("training");

        KTable<String, Long> wordCounts = textLines
                /**
                 * Split the sentences that are produced on the "training" topic and
                 * somehow create a table that holds the count for individual words
                 */

        wordCounts.toStream()
                .peek((key, value) -> LOG.info("{}: {}", key, value))
                .to("words-with-count", Produced.with(Serdes.String(), Serdes.Long()));



        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.start();

    }

}
